#!/bin/bash
# Simple script to convert between RHEL based operating systems
# for the '8' family

function print_error {
  echo $1
  exit
}

RELEASE_PACKAGE_INFO=`yum list installed 2>/dev/null |grep centos.*release`

if [ $? -ne 0 ]; then
  print_error "Unable to detect operating system version - is this a CentOS system?"
fi
if [[ $RELEASE_PACKAGE_INFO == *"stream-release"* ]]; then
  RELEASE="stream"
  DNF_VAR="cernstream8"
elif [[ $RELEASE_PACKAGE_INFO == *"linux-release"* ]]; then
  RELEASE="linux"
  DNF_VAR="cernbase"
else
  RELEASE="unknown"
fi

IS_CERN=0
if [[ $RELEASE_PACKAGE_INFO == *"666"* ]]; then
  IS_CERN=1
fi
if [ $IS_CERN ]; then
  VERSION=`echo $RELEASE_PACKAGE_INFO | awk '{print $2}' | cut -d: -f2`
else
  VERSION=`echo $RELEASE_PACKAGE_INFO | awk '{print $2}' | cut -f2`
fi
if [ ${VERSION:0:1} -ne 8 ]; then
  print_error "Dectected version $VERSION. Only 8 family supported"
fi
if [ "$RELEASE" == "unknown" ]; then
  print_error "Unable to detect operating system version - is this a CentOS system?"
fi

echo "Detected '$RELEASE' version '$VERSION'"


# TODO: add ability to revert changes made below (convert2stream?)

# check dnfvars
DNFVAR_REPOS=`grep $DNF_VAR /etc/yum.repos.d/* | cut -d: -f1 | sort -u | rev | cut -d\/ -f1 | rev | cut -d. -f1`
if [ ! -z "$DNFVAR_REPOS" ]; then
  echo "Detected some repos that are using a CERN DNF variable, disabling them now"
  for REPO in $DNFVAR_REPOS; do
    echo "Disabling $REPO"
    sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/$REPO.repo
  done
fi

# check host can access rhel repos
CAN_ACCESS=`curl https://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel8/8/ --write-out %{http_code} --silent --output /dev/null`
if [ $CAN_ACCESS -ne 200 ]; then
  print_error "Cannot access linuxsoft rhel repos. Perhaps this host needs to be added to the appropriate landb set?"
fi
echo "Configuring RHEL8 repositories"
curl --silent -o /etc/yum.repos.d/rhel8.repo https://linux.web.cern.ch/rhel/repofiles/rhel8.repo
echo "Installing redhat-release, removing centos-release"
dnf --nogpgcheck -y swap centos-release redhat-release
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release

# Estimate space required by distro-sync
# It's impossible to determine this without running dnf, but we can make
# an estimate based on the download size
DOWNLOAD_SIZE_RAW=`dnf --assumeno  --downloadonly distro-sync 2>/dev/null |grep "^Total download size" | cut -d: -f2`
DOWNLOAD_SIZE_VALUE=`echo $DOWNLOAD_SIZE_RAW | awk '{print $1}'`
DOWNLOAD_SIZE_UNIT=`echo $DOWNLOAD_SIZE_RAW | awk '{print $2}'`
AVAILABLE_ROOT_DISKSPACE=`df -B${DOWNLOAD_SIZE_UNIT} / | tail -1 | awk '{print $4}' | sed "s/\$DOWNLOAD_SIZE_UNIT//"`
ESTIMATED_DISKSPACE_REQUIRED=`echo "$DOWNLOAD_SIZE_VALUE*5" | bc`
if [ $AVAILABLE_ROOT_DISKSPACE -gt $ESTIMATED_DISKSPACE_REQUIRED ]; then
  echo "Running distro-sync"
  #TODO: check that this step succeeded ...
  dnf -y distro-sync
else
  print_error "Not enough diskspace is available for the dnf distro-sync transaction"
fi

#TODO: rollback code
# for i in /etc/yum.repos.d/*CERN*.repo.rpmsave; do mv $i `echo $i | sed 's/.rpmsave//'`; done
# rm -f /etc/dnf/protected.d/redhat-release.conf
# dnf --repo=cern-testing swap redhat-release centos-stream-release

IS_BROKEN_PAM=`find /etc/pam.d -xtype l |wc -l`
if [ $IS_BROKEN_PAM -gt 0 ]; then
  # seems to happen only on stream, probably needs a better fix
  echo "pam stack has been broken during upgrade, fixing"
  authselect select sssd with-silent-lastlog --force
fi

echo "removing old centos kernels"
yum list installed  |grep kernel |grep -v rhel-8 | awk '{print $1,$2}' | sed 's/ /-/' | sed 's/.x86_64//' | xargs rpm -e --nodeps

echo "Rebooting system in 30 seconds"
sleep 30
reboot
